const openSidebar = document.querySelector(".collection__filter-sidebar");
const openSidebarBtn = document.querySelector(".filter-sidebar__button");
const openMenu = document.querySelector(".nav__item--menu");
const menu = document.querySelector(".menu");
const collectionWrapper = document.querySelector(".collection__wrapper");


openSidebarBtn.addEventListener("click", function() {
  openSidebar.classList.toggle("collection__filter-sidebar--open");
});

openSidebarBtn.addEventListener("click", function() {
  openSidebarBtn.classList.toggle("filter-sidebar__button--open");
});

openMenu.addEventListener("click", function() {
  menu.classList.toggle("menu--open");
});

openSidebarBtn.addEventListener("click", function() {
  collectionWrapper.classList.toggle("collection__wrapper--open");
});

//preloader


const preloader = document.querySelector(".preloader");
window.addEventListener("load", function() {
  preloader.classList.add("preloader--hiding");

  preloader.addEventListener("transitionend", function() {
    this.classList.remove("preloader--hiding");
    this.classList.add("preloader--hidden");
  });
});



// //pagination

const resultsPerPage = 10;
let page = 1;

const pagination = document.querySelector('.pagination');
const paginationBtn = document.querySelector('.pagination__btn');
const paginationLink = [...document.querySelectorAll('.pagination__link')];



  window.addEventListener("load", () => {


    fetch("https://jsonbox.io/box_e19c6b309a51f5bbc8ab")
      .then(resp => resp.json())
      .then(results => {

        let output = '';
        // const startPoint = (page-1)*resultsPerPage;
        // const endPoint = startPoint + resultsPerPage;
        // const slicedResults = results.slice(startPoint, endPoint);

        for(let i = 0; i<resultsPerPage; i++) {


            output  += `
                <div class="products__box products__box--view">
              <a class="products__link" href="#"><img class="products__img" src="https://via.placeholder.com/150/2U883W" alt="" /></a>

                <div class="products__card">
                <a class="products__link" href="#"><h3 class="products__name">${results[i].title}</h3></a>
                <a class="products__link" href="#"><span class="products__text">${results[i].text}</span></a>
                <span class="products__price">$${results[i].price}</span>
              </div>
              <ul class="products__icon-list">
                <li class="products__item"><a class="products__link" href="#"><svg class="products__icon icon-cart"><use xlink:href="./images/sprites.svg#cart"></use></svg></a></li>
                <li class="products__item"><a class="products__link" href="#"><svg class="products__icon icon-weight"><use xlink:href="./images/sprites.svg#weight"></use></svg></a></li>
                <li class="products__item"><a class="products__link" href="#"><svg class="products__icon icon-heart"><use xlink:href="./images/sprites.svg#heart"></use></svg></a></li>
                <li class="products__item"><a class="products__link" href="#"><svg class="products__icon icon-eye"><use xlink:href="./images/sprites.svg#eye"></use></svg></a></li>
              </ul>
            </div>
            `;

            productsContent.innerHTML = output;
        }



              let paginationOutput = '';



              const pages = Math.ceil(results.length / resultsPerPage);


              for(let i=1; i<=pages; i++) {


                page=i;

                paginationOutput += `<li class="pagination__item"><a class="pagination__link" href="${page}">${page}</a></li>`;


              }


                paginationOutput += `<li class="pagination__item"><a class="pagination__btn button" href="${page+1}">Next</a></li>`;

                pagination.innerHTML = paginationOutput;




              })


              .catch(error => console.log(error));


        });





//search
const search = document.querySelector(".search");
const searchBtn = document.querySelector("#search-btn");
const searchInput = document.querySelector("#search-input");
const productsContent = document.querySelector(".products__content");
searchBtn.addEventListener("click", function(event) {
  event.preventDefault();

  search.classList.toggle("search--open");

});

search.addEventListener("submit", (event) => {
  event.preventDefault();
  const searchPhrase = searchInput.value;

  if (searchPhrase.length < 2 || searchPhrase === '') {
    return;
  }

  fetch("https://jsonbox.io/box_0a9d4affba705f08239c")
    .then(resp => resp.json())
    .then(data => data.filter(result => result.title.toLowerCase().startsWith(searchPhrase.toLowerCase())))
    .then(results => {
      if (results.length > 0) {

        let output = '';
        for(let i = 0; i < results.length; i++) {
          output  += `
              <div class="products__box products__box--view">
            <a class="products__link" href="#"><img class="products__img" src="https://via.placeholder.com/150/2U883W" alt="" /></a>

              <div class="products__card">
              <a class="products__link" href="#"><h3 class="products__name">${results[i].title}</h3></a>
              <a class="products__link" href="#"><span class="products__text">${results[i].text}</span></a>
              <span class="products__price">$${results[i].price}</span>
            </div>
            <ul class="products__icon-list">
              <li class="products__item"><a class="products__link" href="#"><svg class="products__icon icon-cart"><use xlink:href="./images/sprites.svg#cart"></use></svg></a></li>
              <li class="products__item"><a class="products__link" href="#"><svg class="products__icon icon-weight"><use xlink:href="./images/sprites.svg#weight"></use></svg></a></li>
              <li class="products__item"><a class="products__link" href="#"><svg class="products__icon icon-heart"><use xlink:href="./images/sprites.svg#heart"></use></svg></a></li>
              <li class="products__item"><a class="products__link" href="#"><svg class="products__icon icon-eye"><use xlink:href="./images/sprites.svg#eye"></use></svg></a></li>
            </ul>
          </div>
          `;

          productsContent.innerHTML = output;
          search.reset();
        }

      } else {
        console.log('Nie znaleziono wyników dla szukanej frazy');
      }
    })
    .catch(error => console.log(error));
});


//login

const loginContainer = document.querySelector(".login-container");
const login = document.querySelector(".login");
const loginBtn = document.querySelector("#login-btn");
const loginName = document.querySelector("#login-input-name");
const loginPass = document.querySelector("#login-input-password")
const userRow = document.querySelector(".user");
const loginErrorMsg = document.querySelector('.login-errorMsg');

const toggleLogin = () => {
  loginContainer.classList.toggle("login-container--open");
  login.reset();
}

loginBtn.addEventListener("click", toggleLogin);



login.addEventListener("submit", (event) => {
  event.preventDefault();
  const userField = loginName.value;
  const passwordField = loginPass.value;


  fetch("https://jsonbox.io/box_09bf9513cb1f65a88258")
    .then(resp => resp.json())
    .then(data => {

      userLogged = false;
      if(userField !== "" && passwordField !== "") {

            if(data.find(user => user.name === userField && user.password === passwordField)) {


              alert(`${userField} is logged`)

              userLogged = true;
              login.reset();
              loginContainer.classList.remove("login-container--open");

            } else {

              login.reset();

              let output = "Zła nazwa użytkownika lub haslo";
              loginErrorMsg.innerText = output;

          }


        } else {
          return;
        }

          if(userLogged === true) {

          loginBtn.removeEventListener("click", toggleLogin);

          loginBtn.addEventListener("click", function(event) {

            loginContainer.classList.remove("login-container--open");
            userRow.classList.toggle("user--open");

            let output = '';

            output += `<p>Witaj ${userField}</p>`;
            userRow.innerHTML = output;

          });
        }
        })

    .catch(error => console.log(error));
});
